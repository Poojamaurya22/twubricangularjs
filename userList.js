(function(angular) {
  'use strict';
function UserListController($http) {
  var ctrl = this;
  ctrl.count = 0;
  ctrl.max = 10;
  
  $http.get('db.json').then(function(response) {
   ctrl.list = response.data;
});
  
  ctrl.delete = function(a) {
    var idx = ctrl.list.indexOf(a);
    if (idx >= 0) {
      ctrl.list.splice(idx, 1);
    }
  };
  
 ctrl.key = function(event){
      //  alert(event.which);
      if(event.which == 115){ // sort by total score - on press of s
         angular.element(document.querySelector('.score')).triggerHandler('click');
      }else if(event.which == 102){ // on press of f
         angular.element(document.querySelector('.friends')).triggerHandler('click');
      }else if(event.which == 105){ // on press of i
          angular.element(document.querySelector('.influence')).triggerHandler('click');
      }else if(event.which == 99){ // on press of c
          angular.element(document.querySelector('.chirp')).triggerHandler('click');
      }else if(event.which == 32){ // on press of space key - select the element
          ctrl.count = (ctrl.count == ctrl.max)? 1 : ctrl.count+1;
          if(ctrl.count == 1)
            angular.element(document.querySelector('li:nth-child('+ctrl.max+')')).removeClass(' shadow');
          else
            angular.element(document.querySelector('li:nth-child('+(ctrl.count-1)+')')).removeClass(' shadow');
          
          angular.element(document.querySelector('li:nth-child('+ctrl.count+')')).addClass(' shadow');
          
      }
      else if(event.which == 114){ //on press of r - removes the element
          angular.element(document.querySelector('li:nth-child('+ctrl.count+') >  .remove')).triggerHandler('click');
          if(ctrl.count == ctrl.max)
            angular.element(document.querySelector('li:nth-child(1)')).addClass(' shadow');
          else
            angular.element(document.querySelector('li:nth-child('+(ctrl.count+1)+')')).addClass(' shadow');
            
          ctrl.count = (ctrl.count == ctrl.max)? 1 : ctrl.count;
          ctrl.max--;
      }
  }


  ctrl.orderProperty = "";

  ctrl.setOrderProperty = function(propertyName) {
        if (ctrl.orderProperty === propertyName) {
            ctrl.orderProperty = '-' + propertyName;
        } else if (ctrl.orderProperty === '-' + propertyName) {
            ctrl.orderProperty = propertyName;
        } else {
            ctrl.orderProperty = propertyName;
        }
    };

  
}

angular.module('userApp').filter('dateRange', function() {
        return function( items, fromDate, toDate ) {
            var filtered = [];
            var from_date = Date.parse(fromDate);
            var to_date = Date.parse(toDate);
            
            if(fromDate == null && toDate == null){
              angular.forEach(items, function(item) {
                    filtered.push(item);
              });
            }
            else if(fromDate == null && toDate != null){
              angular.forEach(items, function(item) {
                if(item.join_date <= to_date) {
                    filtered.push(item);
                }
            });
            }
            else if(fromDate != null && toDate == null){
              angular.forEach(items, function(item) {
                if(item.join_date >= from_date) {
                    filtered.push(item);
                }
            });
            }
            else{
               angular.forEach(items, function(item) {
                if(item.join_date >= from_date && item.join_date <= to_date) {
                    filtered.push(item);
                }
              });
            }
            return filtered;
        };
    }).component('userList', {
  templateUrl: 'userList.html',
  controller: UserListController
});
})(window.angular);
