# Twubric

Basic - 

1. An AngularJs app with the list of users showing their data and scores fetched from a json file.
2. A button group which sorts that data accordingly.
3. A filter that allows to view the data between a date range.
4. A button for every user to remove that user from the list.

Bonus - 

1. Used flex instead of the basic grid.
2. Added shortcut keyboard keys for sorting, selecting and removing a user.

Shorcut keys - 

s - sorts according to total score (on clicking it again will sort the same in descending order)
f - sorts according to friends (on clicking it again will sort the same in descending order)
i - sorts according to influence (on clicking it again will sort the same in descending order)
c - sorts according to chirpiness (on clicking it again will sort the same in descending order)
space - selects a user
r - removes the selected user from the list


**Note** 

Create and run it on any local server to avoid CORS policy.

